#include <cstdio>
#include <iostream>
#include "windows.h"
#include <vector>

HANDLE MyHandle;
DWORD ThreadID = 0;
HANDLE HookCreateThread(LPTHREAD_START_ROUTINE lpStartAddress, LPVOID lpParameter, LPDWORD lpThreadId);

PVOID EntryPoint()
{
    while (!GetModuleHandleA("d3d11.dll") && !GetModuleHandleA("d3d12.dll"))
        Sleep(1000);
    MessageBoxA(NULL, ("Success Injected Internal"), ("Info"), MB_OK | MB_SYSTEMMODAL);
    CloseHandle(MyHandle);
    return 0;
}

BOOL APIENTRY DllMain(HMODULE hModule,
    DWORD  ul_reason_for_call,
    LPVOID lpReserved
)
{
    if (ul_reason_for_call == DLL_PROCESS_ATTACH) {
        DisableThreadLibraryCalls(hModule);
        MyHandle = HookCreateThread((LPTHREAD_START_ROUTINE)EntryPoint, NULL, &ThreadID);
    }
    return TRUE;
}

DWORD GetModuleSize(DWORD64 base)
{

    IMAGE_DOS_HEADER dos_header = { 0 };
    IMAGE_NT_HEADERS nt_headers = { 0 };
    if (!base)
        return -1;
    dos_header = *(IMAGE_DOS_HEADER*)base;
    nt_headers = *(IMAGE_NT_HEADERS*)(base + dos_header.e_lfanew);
    return nt_headers.OptionalHeader.SizeOfImage;
}
typedef struct
{
    DWORD64 dwEP;
    void* pParam;
} CALL_MYFUNCTION, * PCALL_MYFUNCTION;
typedef DWORD(*_Function)(VOID* p);
PVOID WINAPI HookFunctionThread(PCALL_MYFUNCTION pCMF)
{
    if (pCMF != NULL && pCMF->dwEP != NULL)
    {
        _Function Function = (_Function)pCMF->dwEP;
        Function(pCMF->pParam);
    }
    return NULL;
}
HANDLE HookCreateThread(LPTHREAD_START_ROUTINE lpStartAddress, LPVOID lpParameter, LPDWORD lpThreadId)
{
    HMODULE hNtDll = GetModuleHandleA("ntdll.dll");
    if (hNtDll)
    {
        DWORD dwImageSize = GetModuleSize((DWORD64)hNtDll);
        BYTE* pMemoryData = (BYTE*)hNtDll + dwImageSize - 0x400;

        if (pMemoryData)
        {
            DWORD dwProtect;
            VirtualProtect((LPVOID)pMemoryData, (SIZE_T)0x100, (DWORD)PAGE_EXECUTE_READWRITE, &dwProtect);
            CALL_MYFUNCTION* pCMF = (CALL_MYFUNCTION*)VirtualAlloc((LPVOID)0x00, (SIZE_T)0x100, (DWORD)MEM_COMMIT | (DWORD)MEM_RESERVE, (DWORD)PAGE_EXECUTE_READWRITE);
            pCMF->dwEP = (DWORD64)(lpStartAddress);
            pCMF->pParam = lpParameter;
            memcpy((void*)pMemoryData, (void const*)HookFunctionThread, (size_t)0x100);
            HANDLE hHandle = CreateRemoteThread(GetCurrentProcess(), (LPSECURITY_ATTRIBUTES)NULL, (SIZE_T)0, (LPTHREAD_START_ROUTINE)pMemoryData, (LPVOID)pCMF, (DWORD)NULL, lpThreadId);
            return hHandle;
        }
    }
    return 0;
}